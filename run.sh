#!/usr/bin/env bash

set -euo pipefail

nvim --cmd "set runtimepath^=$(realpath plugin)" "$@"
