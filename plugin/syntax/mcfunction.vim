if exists('b:current_syntax')
    finish
endif

let b:current_syntax = 'mcfunction'

syntax keyword mcfunctionKeyword ability achievement advancement attribute ban ban-ip banlist clear clone data
syntax keyword mcfunctionKeyword debug defaultgamemode deop difficulty effect enchant execute fill gamemode
syntax keyword mcfunctionKeyword gamerule give give help kick kill list locate me op pardon pardon-ip particle
syntax keyword mcfunctionKeyword playsound publish recipe reload save-all save-off save-on say scoreboard seed
syntax keyword mcfunctionKeyword setblock setidletimeout setworldspawn spawnpoint spreadplayers stop summon tag
syntax keyword mcfunctionKeyword tellraw testfor time time tp trigger weather whisper whitelist whitelist
syntax keyword mcfunctionKeyword worldborder xp

highlight link mcfunctionKeyword Keyword

syntax match mcfunctionComment '\v#.*$'
highlight link mcfunctionComment Comment

syntax match mcfunctionNumber '\v<\d+(\.\d*)?>'
highlight link mcfunctionNumber Constant

syntax match mcfunctionSelector '\v\@[aesp]' nextgroup=mcfunctionSelect
syntax region mcfunctionSelect start=/\v\[/ end=/\v\]/ contains=mcfunctionTagName,mcfunctionNumber,mcfunctionJsonObj contained
syntax match mcfunctionTagName '\v\w+\=' contained

highlight link mcfunctionSelector Constant
highlight link mcfunctionSelect Tag
highlight link mcfunctionTagName Keyword

syntax cluster mcfunctionJson contains=mcfunctionJsonObj,mcfunctionJsonList,mcfunctionJsonIdent,mcfunctionJsonString,mcfunctionJsonNumber
syntax region mcfunctionJsonObj start=/\v\{/ end=/\v\}/  contains=@mcfunctionJson contained
syntax region mcfunctionJsonList start=/\v\[/ end=/\v\]/ contains=@mcfunctionJson contained
syntax match mcfunctionJsonIdent '\v\w+:' contained
syntax match mcfunctionJsonNumber '\v<(\d+[bBsSlL]?|\d*\.\d+[fFdD]?)>' contained
syntax region mcfunctionJsonString start=/\v"/ skip=/\v\\/ end=/\v"/ contained

highlight link mcfunctionJsonIdent Function
highlight link mcfunctionJsonString String
highlight link mcfunctionJsonNumber Constant
