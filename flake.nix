{
  description = "vim plugin for .mcfunction";

  inputs = {
    utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, utils }:
    utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages."${system}";
      in
      rec {
        # `nix build`
        packages.vim-mcfunction = pkgs.vimUtils.buildVimPlugin {
          name = "vim-mcfunction";
          src = ./plugin;
        };
        defaultPackage = packages.vim-mcfunction;

        # `nix develop`
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [ openssl ];
          nativeBuildInputs = with pkgs; [ gcc ];
        };
      }
    );
}
