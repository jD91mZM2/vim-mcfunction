# Tag wither roses
tag @e remove wither_rose
tag @e[type=minecraft:item,nbt={OnGround:1b,Item:{id:"minecraft:wither_rose"}}] add wither_rose

# Tag all entities close to wither roses
execute at @e[tag=wither_rose] run tag @e[distance=..1,type=!minecraft:player,tag=!wither_rose] add make_invulnerable

# Make invulnerable
data modify entity @e[tag=make_invulnerable,sort=nearest,limit=1] Invulnerable set value 1
data modify entity @e[tag=make_invulnerable,sort=nearest,limit=1] PersistenceRequired set value 1
attribute @e[tag=make_invulnerable,sort=nearest,limit=1] minecraft:generic.max_health base set 1000

# Give effect
execute at @e[tag=make_invulnerable,sort=nearest,limit=1] anchored eyes positioned ~ ~1 ~ run particle minecraft:heart ~ ~ ~ 0 0 0 1 1 normal
effect give @e[tag=make_invulnerable,sort=nearest,limit=1] glowing 1 0 true
tag @e[tag=make_invulnerable,sort=nearest,limit=1] remove make_invulnerable

# Kill wither roses
kill @e[tag=wither_rose]

# Always heal invulnerable entities
execute as @e[nbt={Invulnerable:1b}] run data modify entity @s Health set value 1000.0
